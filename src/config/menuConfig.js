const menuList = [

    {
        title: '首页',
        key: '/admin/home',
    },
    {
        title: 'UI',
        key: '/admin/ui',
        children: [
            {
                title: 'icon',
                key: '/admin/icon',
            },
            {
                title: 'images',
                key: '/admin/images',
            }
        ]
    },
    {
        title: '表单',
        key: '/admin/form'
    },
    {
        title: '表格',
        key: '/admin/table'
    },
]

export default menuList;