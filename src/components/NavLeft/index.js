
import React, { Component } from 'react'
import { Menu } from 'antd';
import './index.less';

import MenuConfig from '../../config/menuConfig'

const { SubMenu } = Menu;

export default class NavLeft extends Component {

    constructor(props) {
        super(props)
        this.state = {
            menuTreeNodes: ''
        }
    }
    componentDidMount() {
        const menuTreeNodes = this.renderMenu(MenuConfig);
        this.setState({
            menuTreeNodes
        })
    }

    renderMenu = (data) => {
        return data.map((item) => {
            if (item.children) {
                return <SubMenu title={item.title} key={item.key}  >
                    {this.renderMenu(item.children)}
                </SubMenu >
            }
            return <Menu.Item title={item.title} key={item.key} >{item.title}</Menu.Item>
        })
    }

    render() {
        return (
            <div >
                <div className="logo">
                    <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
                    <h1>BOOT</h1>
                </div>
                <Menu theme={'dark'} mode="vertical">
                    {this.state.menuTreeNodes}
                </Menu>
            </div>
        )
    }
}
