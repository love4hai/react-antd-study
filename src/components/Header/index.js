import React, { Component } from 'react'
import { Row, Col } from 'antd'
import './index.less'
import moment from 'moment'
import Axios from 'axios'
// import axios from '../../axios'
// import fetchJsonp from 'fetch-jsonp'

export default class Header extends Component {

    componentWillMount() {
        this.setState({
            userName: '那海'
        })

        setInterval(() => {
            let systemTime = moment().format('YYYY年MM月DD日 HH:mm:ss')
            this.setState({
                systemTime
            })
        }, 1000)


        this.getWeatherAPIData()
    }


    getWeatherAPIData() {
        let city = "深圳"

        let _this= this
        Axios.get('http://api.help.bj.cn/apis/weather2d/', {
            params: {
                id: city
            }
        }).then(function (res) {
            //console.log(res)
            let weather = res.data.weather
            let datePictureUrl = res.data.weatherimg
            _this.setState({
                weather,
                datePictureUrl
            })
        });
    }

    render() {
        return (
            <div className="header">
                <Row className="header-top">
                    <Col span={24} >
                        <span>欢迎，{this.state.userName}</span>
                        <a href="#">退出</a>
                    </Col>
                </Row>
                <Row className="breadcrumb">
                    <Col span={4} className="breadcrumb-title">
                        <span>首页</span>
                    </Col>
                    <Col span={20} className="weather">
                        <span className="date">{this.state.systemTime}</span>
                        <span className="weather-img">
                            <img src={this.state.datePictureUrl} alt='天气图片' />
                        </span>
                        <span className="weather-detail">
                            {this.state.weather}
                        </span>
                    </Col>
                </Row>
            </div>
        )
    }
}
