import JsonP from 'jsonp'
export default class Axios{
    static jsonp(options){
      return new Promise((resolve,reject)=>{
        JsonP(options.url,{
          param:''
        },function(err,response){
          if(response.status ==='success'){
            resolve(response)
            console.log(response)
          }else{
            reject(response.message)
          }
        })
      })
    }
  }

